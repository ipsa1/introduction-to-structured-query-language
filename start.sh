#!/usr/bin/env bash

sudo mkdir -p var
sudo chmod a+w var

sudo mkdir -p var/data/mysql
sudo chown 999:999 var/data/mysql

sudo mkdir -p var/storage

sudo chmod g+w -R .

sudo chmod a+x ./bin/composer.phar

docker-compose up -d --remove-orphans

until curl --http0.9 --output curl.txt http://localhost:3301/; do
  echo "..."
  sleep 1
done

rm curl.txt

sudo chown -R :www-data .
sudo chmod g+w -R .

